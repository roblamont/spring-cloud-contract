package com.retailbank.creditcheckservice;

import org.junit.Before;
import io.restassured.module.mockmvc.RestAssuredMockMvc;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BaseContractTest {

    @Before
    public void setup() {
        final CreditCheckService creditCheckService = mock(CreditCheckService.class);
        when(creditCheckService.doCreditCheck(1234)).thenReturn(new CreditCheckResponse(CreditCheckResponse.Score.HIGH));
        RestAssuredMockMvc.standaloneSetup(new CreditCheckController(creditCheckService));
    }

}
