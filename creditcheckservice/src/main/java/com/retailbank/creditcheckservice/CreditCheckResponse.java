package com.retailbank.creditcheckservice;

public class CreditCheckResponse {

    private Score score;

    public CreditCheckResponse(Score high) {
        score = high;
    }

    public enum Score {
        HIGH
    }

    public Score getScore() {
        return score;
    }

    public void setScore(Score score) {
        this.score = score;
    }
}
